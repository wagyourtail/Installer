package me.deftware.aristois.installer;

import joptsimple.OptionParser;
import joptsimple.OptionSet;
import joptsimple.OptionSpec;
import me.deftware.aristois.installer.jsonbuilder.AbstractJsonBuilder;
import me.deftware.aristois.installer.ui.InstallerUI;
import org.apache.commons.io.FileUtils;

import javax.swing.*;
import java.io.File;
import java.io.IOException;
import java.net.URLClassLoader;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.jar.Attributes;
import java.util.jar.Manifest;
import java.util.zip.ZipEntry;
import java.util.zip.ZipOutputStream;

/**
 * @author Deftware
 */
public class Main {

	public static void main(String[] args) {
		OptionParser parser = new OptionParser();
		OptionSpec<String> dataUrl = parser.accepts("data").withOptionalArg();
		OptionSpec<String> mvnUrl = parser.accepts("mvn").withOptionalArg();
		OptionSpec<Boolean> betaVersions = parser.accepts("beta").withOptionalArg().ofType(Boolean.class).describedAs("bool");
		OptionSpec<Boolean> gen = parser.accepts("gen").withOptionalArg().ofType(Boolean.class).describedAs("bool");
		OptionSpec<Boolean> universal = parser.accepts("universal").withOptionalArg().ofType(Boolean.class).describedAs("bool");
		parser.allowsUnrecognizedOptions();
		OptionSet optionSet = parser.parse(args);
		if (optionSet.has(dataUrl)) {
			InstallerAPI.dataUrl = optionSet.valueOf(dataUrl);
			InstallerAPI.getLogger().info("Using custom data url {}", InstallerAPI.dataUrl);
		}
		if (optionSet.has(mvnUrl)) {
			InstallerAPI.aristoisMaven = optionSet.valueOf(mvnUrl);
			InstallerAPI.getLogger().info("Using custom data url {}", InstallerAPI.aristoisMaven);
		}
		if (optionSet.has(universal)) {
			InstallerAPI.universal = optionSet.valueOf(universal);
		}
		try {
			Attributes attr =
					new Manifest(((URLClassLoader) InstallerAPI.class.getClassLoader()).findResource("META-INF/MANIFEST.MF").openStream()).getMainAttributes();
			InstallerAPI.donorBuild = Boolean.parseBoolean(attr.getValue("donorBuild"));
		} catch (IOException E) {
			E.printStackTrace();
		}
		try {
			UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());
			new Thread(() -> {
				try {
					Thread.currentThread().setName("Version fetcher/parser");
					InstallerAPI.fetchData(optionSet.has(betaVersions));
					if (optionSet.has(gen)) {
						InstallerAPI.donorBuild = false;
						InstallerAPI.populateVersions(optionSet.has(betaVersions));
						genConfigs(args);
						InstallerAPI.donorBuild = true;
						InstallerAPI.populateVersions(optionSet.has(betaVersions));
						genConfigs(args);
					} else {
						InstallerAPI.populateVersions(optionSet.has(betaVersions));
						InstallerUI.create().setVisible(true);
					}
				} catch (Exception ex) {
					ex.printStackTrace();
				}
			}).start();
		} catch (Exception ex) {
			ex.printStackTrace();
		}
	}

	private static void genConfigs(String[] args) {
		InstallerAPI.getLogger().info("== Generating launch configs for all supported versions ==");
		File path = new File(System.getProperty("user.dir") + File.separator + "configs" + File.separator);
		if (!path.exists() && !path.mkdirs()) {
			InstallerAPI.getLogger().error("Failed to create dir {}", path.getAbsolutePath());
		}
		File tempDir = new File(path.getAbsolutePath() + File.separator + "versions" + File.separator + "temp");
		if (!tempDir.mkdirs()) {
			InstallerAPI.getLogger().error("Could not create {}", tempDir.getAbsolutePath());
		}
		InstallerAPI.	getLogger().info("Saving files to {}", path.getAbsolutePath());
		InstallerAPI.versions.values().forEach(v -> {
			AbstractJsonBuilder builder = v.getBuilder("", "Vanilla");
			InstallerAPI.getLogger().info(builder.install(builder.build(v), v, path.getAbsolutePath() + File.separator));
			try {
				File versionsFolder = new File(path.getAbsolutePath() + File.separator + "versions" + File.separator + v.getVersion() + "-Aristois");
				File tempDirVersion = new File(path.getAbsolutePath() + File.separator + "versions" + File.separator + "temp" + File.separator + v.getVersion() + "-Aristois");
				FileUtils.copyDirectory(versionsFolder, tempDirVersion);
				FileUtils.copyFile(new File(args[1]), new File(path.getAbsolutePath() + File.separator + "versions" + File.separator + "temp" + File.separator + v.getVersion() + "-Aristois" + File.separator + "Readme.txt"));
				pack(tempDir.getAbsolutePath(), path.getAbsolutePath() + File.separator + "versions" + File.separator + v.getVersion() + "-Aristois-Latest" + (InstallerAPI.donorBuild ? "-D" : "") + ".zip");
				FileUtils.deleteDirectory(tempDirVersion);
			} catch (Exception ex) {
				ex.printStackTrace();
			}
		});
		try {
			FileUtils.deleteDirectory(tempDir);
		} catch (Exception ex) {
			ex.printStackTrace();
		}
	}

	private static void pack(String sourceDirPath, String zipFilePath) throws IOException {
		Path p = Files.createFile(Paths.get(zipFilePath));
		try (ZipOutputStream zs = new ZipOutputStream(Files.newOutputStream(p))) {
			Path pp = Paths.get(sourceDirPath);
			Files.walk(pp)
					.filter(path -> !Files.isDirectory(path))
					.forEach(path -> {
						ZipEntry zipEntry = new ZipEntry(pp.relativize(path).toString().replace("\\", "/"));
						try {
							zs.putNextEntry(zipEntry);
							Files.copy(path, zs);
							zs.closeEntry();
						} catch (IOException e) {
							e.printStackTrace();
						}
					});
		}
	}

}
